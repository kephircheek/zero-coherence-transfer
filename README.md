# Transfer of 0-order coherence matrix along spin-1/2 chain

There is a large class of quantum states which include only 0-order coherence matrix.
Requiring the perfect transfer of such states we fix in a certain way all the elements in the density matrices of the initial sender state.
In certain cases, to provide such transfer, some elements of the  sender's initiual density  matrix must be zero. 
Then, to create correspondent zero elements in the receiver's density matrix at the required  time instant 
we have to implement the extended receiver technique with the unitary transformation.
The asymptotic of teh pointed above perfectly transferable 0-order coherence matrix for long chain is discussed and deviation from this asymptotic is studied for a finite chain is studied.
The problem of arbitrary parameter transfer via 0-order coherence matrix is also considered.
